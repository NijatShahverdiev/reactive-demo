FROM openjdk:17-jdk-slim
WORKDIR /app
COPY build/libs/reactive-demo-0.1.jar app.jar
EXPOSE 8082
ENTRYPOINT ["java", "-jar", "/app/app.jar"]