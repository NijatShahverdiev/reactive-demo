CREATE TABLE IF NOT EXISTS customer
(
    id       UUID default  random_uuid() PRIMARY KEY,
    name VARCHAR(255)
);



CREATE TABLE IF NOT EXISTS customer_cif
(
    id       UUID default  random_uuid() PRIMARY KEY,
    cif VARCHAR(255),
    customer_id UUID
);

