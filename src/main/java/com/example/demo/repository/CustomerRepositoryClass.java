package com.example.demo.repository;

import com.example.demo.domain.CustomerCifDto;
import com.example.demo.domain.CustomerDto;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.repository.query.Param;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.BiFunction;

@Repository
@RequiredArgsConstructor
@Slf4j
public class CustomerRepositoryClass {

    private final DatabaseClient databaseClient;
    private final ConnectionFactory connectionFactory;

    public Flux<CustomerDto> findAll() {
        return databaseClient
                .sql("select * from customer")
                .map(MAPPING_FOR_FIRST)
                .all();
    }

    public Mono<CustomerDto> findById(@Param("id") UUID id) {
        String sql = """
                    select c.id as customer_id, c.name as customer_name, cc.id as cif_id, cc.cif as cif_value
                        from customer c
                        left join customer_cif cc on cc.customer_id = c.id
                    where c.id = $1
                """;
        return databaseClient
                .sql(sql)
                .bind("$1", id)
                .map(MAPPING_FOR_JOIN)
                .all()
                .collectList()
                .flatMap(tuple2s -> {
                    if (!tuple2s.isEmpty()) {
                        CustomerDto customer = tuple2s.get(0).getT1();
                        List<CustomerCifDto> cifs = new ArrayList<>();
                        for (Tuple2<CustomerDto, CustomerCifDto> tuple2 : tuple2s) {
                            CustomerCifDto cifDto = tuple2.getT2();
                            cifs.add(cifDto);
                        }
                        customer.setCustomerCifs(cifs);
                        return Mono.just(customer);
                    } else {
                        return Mono.empty();
                    }
                });
    }

    public Mono<CustomerDto> createCustomer(CustomerDto customer) {
        return databaseClient
                .sql("insert into customer (name) values ($1)")
                .bind("$1", customer.getName())
                .fetch()
                .first()
                .map(c -> CustomerDto.builder()
                        .id((UUID) c.get("id"))
                        .build()
                );
    }

    private static final BiFunction<Row, RowMetadata, Tuple2<CustomerDto, CustomerCifDto>> MAPPING_FOR_JOIN =
            (row, rowMetaData) -> {
                CustomerDto customerDto = CustomerDto.builder()
                        .id(row.get("customer_id", UUID.class))
                        .name(row.get("customer_name", String.class))
                        .build();
                CustomerCifDto customerCifDto = CustomerCifDto.builder()
                        .id(row.get("cif_id", UUID.class))
                        .cif(row.get("cif_value", String.class))
                        .build();
                return Tuples.of(customerDto, customerCifDto);

            };

    private static final BiFunction<Row, RowMetadata, CustomerDto> MAPPING_FOR_FIRST =
            (row, rowMetaData) -> CustomerDto.builder()
                    .id(row.get("id", UUID.class))
                    .name(row.get("name", String.class))
                    .build();
}
