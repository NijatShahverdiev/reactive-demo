package com.example.demo.repository;

import com.example.demo.domain.CustomerCif;
import com.example.demo.domain.CustomerCifDto;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import lombok.RequiredArgsConstructor;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;
import java.util.function.BiFunction;

@Repository
@RequiredArgsConstructor
public class CifRepository {

    private final DatabaseClient databaseClient;

    public static final BiFunction<Row, RowMetadata, CustomerCifDto> MAPPING_FUNCTION =
            (row, rowMetaData) -> CustomerCifDto.builder()
                    .id(row.get("id", UUID.class))
                    .cif(row.get("cif", String.class))
                    .customerId(row.get("customer_id", UUID.class))
                    .build();

    public Flux<CustomerCifDto> findAll() {
        return databaseClient
                .sql("select * from customer_cif")
                .map(MAPPING_FUNCTION)
                .all();
    }

    public Mono<CustomerCifDto> createCif(CustomerCifDto customerCif){
        return databaseClient
                .sql("insert into customer_cif (cif, customer_id) values ($1, $2)")
                .bind("$1", customerCif.getCif())
                .bind("$2", customerCif.getCustomerId())
                .map(MAPPING_FUNCTION)
                .one();
    }
}
