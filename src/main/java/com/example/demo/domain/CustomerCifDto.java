package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerCifDto {
    private UUID id;
    private String cif;
    private UUID customerId;

    public CustomerCifDto(UUID id, String cif){
        this.id = id;
        this.cif = cif;
    }
}
