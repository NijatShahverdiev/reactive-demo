package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.UUID;

@Data
@Builder
public class CustomerCif {
    @Id
    private UUID id;
    private String cif;
    private UUID customerId;
}
