package com.example.demo.domain;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

import java.util.UUID;

@Data
public class Customer {
    @Id
    private UUID id;
    private String name;
}
