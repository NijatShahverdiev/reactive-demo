package com.example.demo.service;

import com.example.demo.domain.Customer;
import com.example.demo.domain.CustomerDto;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.repository.CustomerRepositoryClass;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CustomreService {
    private final CustomerRepository customerRepository;

 /*   public Flux<CustomerDto> findAll(){
        return customerRepository.findAll();
    }

    public Mono<CustomerDto> findById(UUID customerId) {
        return customerRepository.findById(customerId);
    }

    public Mono<CustomerDto> create(CustomerDto customer){
        return customerRepository.createCustomer(customer);
    }*/

    public Mono<Customer> create(Customer customer){
        return customerRepository.save(customer);
    }

    public Mono<Customer> findById(UUID customerId) {
        return customerRepository.findById(customerId);
    }
}
