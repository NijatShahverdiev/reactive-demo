package com.example.demo.controller;

import com.example.demo.domain.Customer;
import com.example.demo.domain.CustomerDto;
import com.example.demo.service.CustomreService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/customers")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CustomerController {
    final CustomreService customreService;

/*    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Flux<CustomerDto> findAll(){
        return customreService.findAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<CustomerDto> findById(@PathVariable("id") UUID userId){
        return customreService.findById(userId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<CustomerDto> create(@RequestBody CustomerDto customer){
        return customreService.create(customer);
    }*/

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Mono<Customer> findById(@PathVariable("id") UUID userId){
        return customreService.findById(userId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Customer> create(@RequestBody Customer customer){
        return customreService.create(customer);
    }

}
