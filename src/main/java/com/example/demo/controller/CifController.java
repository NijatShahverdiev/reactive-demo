package com.example.demo.controller;

import com.example.demo.domain.CustomerCif;
import com.example.demo.domain.CustomerCifDto;
import com.example.demo.repository.CifRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/cifs")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CifController {

    final CifRepository cifRepository;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Flux<CustomerCifDto> findAll(){
        return cifRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<CustomerCifDto> create(@RequestBody CustomerCifDto customerCif){
        return cifRepository.createCif(customerCif);
    }
}
